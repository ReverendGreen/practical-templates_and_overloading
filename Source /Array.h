//
//  Array.h
//  CommandLineTool
//
//  Created by Neil McGuiness on 26/09/2016.
//  Copyright (c) 2016 Tom Mitchell. All rights reserved.
//

#ifndef CommandLineTool_Array_h
#define CommandLineTool_Array_h
template <class Type>
class Array

{
public:
    
    Array()
    {
        arraySize = 0;
        fArrayPoint = nullptr;
    }
    
    /** Destructor to delete the memory */
    ~Array()
    {
        delete [] fArrayPoint;
    }
    
    void add (Type itemValue)// Mutator - adds an item to the array by copying, resizing and deleting the old array.
    
    {
        /** Incrementing the array size up one as we get a new value */
        arraySize +=1;
        
        
        
        if (fArrayPoint != nullptr)
            
        {
            //Creating temporary array for copying.
            Type tempArray[arraySize - 1];
            
            //Because array size is incremented every time so is one ahead we need to only copy up to the next to last element in the array, hence < arraySize - 1.
            for (int x = 0; x < arraySize - 1; x ++)
            {
                tempArray[x] = fArrayPoint[x];
            }
            
            // releasing the old memory to make room for allocating new memory.
            delete fArrayPoint;
            fArrayPoint = new Type[arraySize];
            
            // copying the temp array into the new one.
            for (int x = 0; x < arraySize - 1; x ++)
            {
                fArrayPoint[x] = tempArray[x];
            }
            // copying the item value (passed into function) into the newest element in the array (-1 because elemnt access is always one below [remember the first element of an array of size 1 : array[1] is accessed by array[0]))
            fArrayPoint[arraySize - 1] = itemValue;
            
            
        }
        else // Pointer has not been dynamiclly allocated yet
            
            
        {
            // So we allocate here.
            fArrayPoint = new Type[arraySize];
            // And add the itemvalue to the first element of the array (0, which is why it has to be arraySize - 1)
            fArrayPoint[arraySize - 1] = itemValue;
            
        }
        
    }
    
    Type get (int index) const // Accessor
    
    {
        return fArrayPoint[index];
    }
    
    Type size() const//Accesor
    
    {
        return arraySize;
    }
    
    // Attempting to overload operator. -
    // Returns the type of class that this has been specified to be.
    //Notice special operator keyword to signify the operator overloading.
    Type operator[](int index)
    
    {
        // Catching invalid input.
        if (index < 0 || index > arraySize - 1)
        
        {
            std::cout << "Index out of range / invalid \n\n";
            return NULL;
        }
        
        else
        {
            return fArrayPoint[index];

        }
    }
    
    // Overloading the comparative operator to compare arrays of my class type.
    bool operator==(const Array& otherArray)
    {
        // Checking the sizes match to begin with.
        if (size() == otherArray.size())
        {
            // If so count through the arrays to check that they are identical.
            for (int i = 0 ; i < arraySize; i ++)
            
            {
                // Checking that the arrays match.
                if (get(i) == otherArray.get(i))
                {
                    return true;
                }
                
            }
        }
        // If not then function will reach the return of false.
        return false;
    }
    
    
private:
    
    Type* fArrayPoint;
    
    int arraySize;
    
   
};
#endif
