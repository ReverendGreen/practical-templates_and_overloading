//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "Array.h"

bool testArray(); //function to test Array class

int main (int argc, const char* argv[])
{
    
    if (testArray() == true)
    {
        std::cout << "all Array tests passed\n\n";
    }
    else
    {
        std::cout << "Array tests failed\n";
    }

    return 0;
}

bool testArray()
{
    Array<int> iArray, iArray2, iArray3;
    const int testArray[] = {6,7,8,9,10,11};
    const int testArraySize = sizeof (testArray) / sizeof (testArray[0]);
    int userChoice;
    
    if (iArray.size() != 0)
    {
        std::cout << "size is incorrect\n";
        return false;
    }
    
    for (int i = 0; i < testArraySize; i++)
    {
        iArray.add (testArray[i]);
        iArray2.add(testArray[i]);
        iArray3.add(testArray[i] + 23);
        
        if (iArray.size() != i + 1)
        {
            std::cout << "size is incorrect\n";
            return false;
        }
        
        if (iArray[i] != testArray[i])
        {
            std::cout << "value at index "<< i << " recalled incorrectly\n" ;
            return false;
        }
        
    }
    
    std::cout << " Enter index that you wish to see result of: \n";
    std::cin >> userChoice;
    std::cout << iArray[userChoice] << " is the result of that. \n\n";
    
    std::cout << " Checking to see if Arrays match...: \n";
    
    if (iArray == iArray2)
    
    {
        std::cout << " iArray1 and iArray2 are completely identical! \n";

    }
    else
    {
      std::cout << " iArray1 and iArray2 do not match \n";
    }
    
    if (iArray == iArray3)
        
    {
        std::cout << " iArray1 and iArray3 are completely identical! \n";
        
    }
    else
    {
        std::cout << " iArray1 and iArray3 do not match \n";
    }
    
    

    
    
//    //removing first
//    array.remove (0);
//    if (array.size() != testArraySize - 1)
//    {
//        std::cout << "with size after removing item\n";
//        return false;
//    }
//    
//    for (int i = 0; i < array.size(); i++)
//    {
//        if (array.get(i) != testArray[i+1])
//        {
//            std::cout << "problems removing items\n";
//            return false;
//        }
//    }
//    
//    //removing last
//    array.remove (array.size() - 1);
//    if (array.size() != testArraySize - 2)
//    {
//        std::cout << "with size after removing item\n";
//        return false;
//    }
//    
//    for (int i = 0; i < array.size(); i++)
//    {
//        if (array.get(i) != testArray[i + 1])
//        {
//            std::cout << "problems removing items\n";
//            return false;
//        }
//    }
//    
//    //remove second item
//    array.remove (1);
//    if (array.size() != testArraySize - 3)
//    {
//        std::cout << "with size after removing item\n";
//        return false;
//    }
//    
//    if (array.get (0) != testArray[1])
//    {
//        std::cout << "problems removing items\n";
//        return false;
//    }
//    
//    if (array.get (1) != testArray[3])
//    {
//        std::cout << "problems removing items\n";
//        return false;
//    }
//    
//    if (array.get (2) != testArray[4])
//    {
//        std::cout << "problems removing items\n";
//        return false;
//    }
    
    return true;
}
