//
//  Array.h
//  CommandLineTool
//
//  Created by Neil McGuiness on 26/09/2016.
//  Copyright (c) 2016 Tom Mitchell. All rights reserved.
//

#ifndef CommandLineTool_Array_h
#define CommandLineTool_Array_h

class Array

{
public:
    
    /** Constructor to dynamically allocated the floating point array */
    Array();
    
    /** Destructor to delete the memory */
    ~Array();
    
    void add (float itemValue); // Mutator - adds an item to the array by copying, resizing and deleting the old array.
    
    float get (int index); // Accessor
    
    int size(); //Accesor
    
    
    
private:
    
    float* fArrayPoint;
    
    int arraySize;
    
   
};
#endif
