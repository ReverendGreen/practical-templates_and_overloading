//
//  LinkedList.cpp
//  
//
//  Created by Neil McGuiness on 27/09/2016.
//
//

#include <stdio.h>
#include "LinkedList.h"

/** My constructor initialises the head to point to nullptr */

linkedList::linkedList()

{
    head = nullptr;

}

/** the destructor deletes all Nodes in the list */
linkedList:: ~linkedList()

{
    
}

/** adds new items to the end of the array */
void linkedList:: add (float itemValue)

{
    // Operation for creating new elements in a linked list.
    // We create a new pointer and allocated memory for a Node and point to it.
    Node* newNode = new Node();
    
    // Using the (->) syntax we can write the current itemValue (function input) into the value block of our newNode
    //And we can point this newNode linkpointer (next) to NULL, signifying the end of the list.
    newNode->value = itemValue;
    newNode->next = nullptr;
    
    if (head == nullptr)
    {
        head = newNode;
        
    }
    else
    {
        Node* temp = head;
        while (temp->next != nullptr)
        {
            temp = temp->next;
        }
        temp->next = newNode;
    }
    
    

    
}


/**  returns the value stored in the Node at the specified index */
float linkedList:: get (int index)

{
    int counter = 0;
    float nodeValue = 0;
    
    
    
    
    if (head == nullptr)
    {
        return NULL;
    }
    
    else
    {
        Node* temp = head;
        
        while (temp != nullptr)
        {
            temp = temp->next;
            counter ++;
            
            if (counter == index)
            {
                nodeValue = temp->value;
            }
        }
    }
    
    
    return nodeValue;
    
}

/** returns the size of the linked list */
int linkedList:: size()

{
    int counter = 0;
    
    
  if (head == nullptr)
        
    {
        return 0;
    }
    
    else
        
    {
        
        Node* temp = head;
        while (temp != nullptr)
        {
            temp = temp->next;
            counter ++;
        }
    }
    
    return counter;

}
