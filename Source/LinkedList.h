//
//  LinkedList.h
//  CommandLineTool
//
//  Created by Neil McGuiness on 27/09/2016.
//  Copyright (c) 2016 Tom Mitchell. All rights reserved.
//

#ifndef CommandLineTool_LinkedList_h
#define CommandLineTool_LinkedList_h

class linkedList

{
    
public:
    
    /** My constructor initialises the head to point to nullptr */
    
    linkedList();
    
    /** the destructor deletes all Nodes in the list */
    ~linkedList();
    
    /** adds new items to the end of the array */
    void add (float itemValue);
    
    
    /**  returns the value stored in the Node at the specified index */
    float get (int index);
    
    /** returns the size of the linked list */
    int size();
    
private:
    
    struct Node
    
    {
        float value;
        Node* next;

    };
    
    Node* head;

    
    
};
#endif
