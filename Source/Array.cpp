//
//  Array.cpp
//  
//
//  Created by Neil McGuiness on 26/09/2016.
//
//

#include <stdio.h>
#include "Array.h"


/** Constructor to dynamically allocated the floating point array */
Array :: Array()
{
    arraySize = 0;
    fArrayPoint = nullptr;
}

/** Destructor to delete the memory */
Array :: ~Array()
{
    delete [] fArrayPoint;
}

void Array::add (float itemValue)// Mutator - adds an item to the array by copying, resizing and deleting the old array.

{
    /** Incrementing the array size up one as we get a new value */
    arraySize +=1;
    
    
    /** Checking if the fArrayPointer has been allocated yet or is emty (NULL) */
    if (fArrayPoint != nullptr)
    
    {
        //Creating temporary array for copying.
        float tempArray[arraySize - 1];
        
        //Because array size is incremented every time so is one ahead we need to only copy up to the next to last element in the array, hence < arraySize - 1.
        for (int x = 0; x < arraySize - 1; x ++)
        {
            tempArray[x] = fArrayPoint[x];
        }
        
        // releasing the old memory to make room for allocating new memory.
        delete fArrayPoint;
        fArrayPoint = new float[arraySize];
        
        // copying the temp array into the new one.
        for (int x = 0; x < arraySize - 1; x ++)
        {
            fArrayPoint[x] = tempArray[x];
        }
        // copying the item value (passed into function) into the newest element in the array (-1 because elemnt access is always one below [remember the first element of an array of size 1 : array[1] is accessed by array[0]))
        fArrayPoint[arraySize - 1] = itemValue;
    }
    
    else // Pointer has not been dynamiclly allocated yet
    {
        // So we allocate here.
        fArrayPoint = new float[arraySize];
        // And add the itemvalue to the first element of the array (0, which is why it has to be arraySize - 1)
        fArrayPoint[arraySize - 1] = itemValue;

    }
    
    
    // HOW DO I COPY THE OTHER ARRAY INTO THE NEW ONE WITHOUT COPYING IT FIRST INTO A LOCAL ARRAY??
    
}

float Array::get (int index) // Accessor

{
    return fArrayPoint[index];
}

int Array::size() //Accesor

{
    return arraySize;
}