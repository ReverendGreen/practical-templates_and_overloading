//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "LinkedList.h"


template <typename Type>
Type max(Type a, Type b)

{
    std::cout << "max :\n";
    
    if (a < b)
    {
        return b;
    }
    else
    {
        return a;
    }
}

template <typename Type>
Type min(Type a, Type b)

{
    std::cout << "max :\n";
    
    if (a < b)
    {
        return a;
    }
    else
    {
        return b;
    }
}


template <typename Type>
void print(Type array[], int arraySize)

{
    for (int x = 0; x < arraySize; x ++)
    
    {
        std::cout << "Array element : " << x << " Value is : " << array[x]<< std::endl;

    }
    
}

template <typename Type>
double getAverage(Type array[], int arraySize)

{
    double runningTot = 0;
    
    std::cout << "Average of Array is : " ;
    
    for (int x = 0; x < arraySize; x ++)
        
    {
        runningTot += array[x];
    }
    
    return runningTot / arraySize;
}

int main (int argc, const char* argv[])
{
    float myArray[8]= {440.0, 880.0, 356.5, 654.8, 555.7, 987.0, 342.1, 687.9};
    
    std:: cout << max(56, 987) << std:: endl;
    std:: cout <<  min(65.98f, 456.9f)<< std:: endl;
    print(myArray, 8);
    std:: cout <<  getAverage(myArray, 8) << std:: endl;

    return 0;
}


